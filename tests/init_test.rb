# -*- coding: utf-8 -*-

require 'minitest/autorun'
require '../bin/initial.rb'

include Oyun

# Oyuncu isimli class ile ilgili testler
describe Oyuncu do
  before do
    @oyuncu = yeni_oyuncu(filename: '../data/default.yaml')
  end

  describe 'Yeni oyuncu yaratildiginda' do
    it 'Nesne 5 instance değişken içerir' do
      @oyuncu.must_be_kind_of Object
      @oyuncu.id.must_be_kind_of Integer
      @oyuncu.zar.must_be_kind_of Integer
      @oyuncu.veriler.must_be_kind_of Hash
      @oyuncu.veriler[:isim].must_equal 'Sabah Yıldızı'
      @oyuncu.veriler[:saglik].must_equal 100
      @oyuncu.veriler[:silah][:model].must_equal 'vx-20'
      @oyuncu.veriler[:silah][:tip].must_equal 'Işın Kılıcı'
      @oyuncu.veriler[:silah][:atak].must_equal 3
      @oyuncu.basari_katsayisi.must_be_kind_of Integer
      @oyuncu.canta.must_be_kind_of Hash
    end
  end

  describe 'Zar attigim zaman' do
    it 'Bana 0 ile 40 arası zar donmeli, basarim 0 ile 4 arasi' do
      @oyuncu.basarim.must_be :>=, 0 || :<=, 4
    end
  end

  describe 'Oyuncu etkilesime girdiginde' do
    it 'Hasar alma iyilesme ve hayatta mi durumlari icin' do
      @oyuncu.hayatta_mi?.must_equal true
      @oyuncu.darbe_aldi(50)
      @oyuncu.veriler[:saglik].must_equal 50
      @oyuncu.iyilestir(10)
      @oyuncu.veriler[:saglik].must_equal 60
      @oyuncu.darbe_aldi(60)
      @oyuncu.veriler[:saglik].must_equal 0
      @oyuncu.hayatta_mi?.must_equal false
      @oyuncu.saldir.must_be :>=, 0 || :<=, 12
    end
  end

  describe 'Yeni item yaratildiginda' do
    before do
      @nesne = Item.new('hurda')
    end

    describe 'Item nesnesi oluşturulduğunda' do
      it 'bir sembol türündedir' do
        @nesne.must_be_kind_of Object
        @nesne.etkilesim(@oyuncu)
        @oyuncu.canta.key?(@nesne.malzeme).must_equal true
        @oyuncu.canta[@nesne.malzeme].must_equal 1
        @nesne.etkilesim(@oyuncu)
        @oyuncu.canta[@nesne.malzeme].must_equal 2
      end
    end
  end
end

# Dusman sinifi ile ilgili testler
describe Dusman do
  before do
    @dusman = yeni_dusman(filename: '../data/enemy.yaml')
    @savasci = yeni_oyuncu(filename: '../data/default.yaml')
  end

  describe 'Yeni düşman yaratıldığında' do
    it 'Bana 5 instance değişken döner' do
      @dusman.must_be_kind_of Object
      @dusman.id.must_be_kind_of Integer
      @dusman.zar.must_be_kind_of Integer
      @dusman.veriler.must_be_kind_of Hash
      @dusman.veriler[:isim].must_equal 'Düşman'
      @dusman.veriler[:saglik].must_equal 100
      @dusman.veriler[:silah][:model].must_equal 'vx-20'
      @dusman.veriler[:silah][:tip].must_equal 'Işın Kılıcı'
      @dusman.veriler[:silah][:atak].must_equal 0
      @dusman.basari_katsayisi.must_be_kind_of Integer
      @dusman.canta.must_be_kind_of Hash
      @dusman.etkilesim(@savasci)
    end
  end

  describe 'Dusman zar attigi zaman' do
    it 'Bana 0 ile 40 arası zar donmeli, basarim 0 ile 4 arasi' do
      @dusman.basarim.must_be :>=, 0 || :<=, 4
    end
  end

  describe 'Düşam etkileşime girdiğinde' do
    it 'Hasar alma iyileşme ve hayatta mı durumları için' do
      @dusman.hayatta_mi?.must_equal true
      @dusman.darbe_aldi(50)
      @dusman.veriler[:saglik].must_equal 50
      @dusman.iyilestir(10)
      @dusman.veriler[:saglik].must_equal 60
      @dusman.darbe_aldi(60)
      @dusman.veriler[:saglik].must_equal 0
      @dusman.hayatta_mi?.must_equal false
      @dusman.saldir.must_be :>=, 0 || :<=, 12
    end
  end
end

# Oyuncu isimli modul ile ilgi testler
describe Oyun do
  describe 'yeni_oyuncu metodunu çağırdığımda' do
    it 'bana bir nesne döner:' do
      yeni_oyuncu(filename: '../data/default.yaml').must_be_kind_of Object
    end
  end

  describe 'Oyun verisi ile ilgili islemler' do
    it 'Oyunu kaydet dedigimde' do
      kaydet(@oyuncu, yol: '../data/', filename: 'test').must_equal 'Saved!'
    end

    it 'Oyunu yükle dediğimde' do
      yukleme_datasi = Oyun.yukle('../data/Save-test.yaml')
      yukleme_datasi.must_be_kind_of Object unless yukleme_datasi.nil?
    end
  end
end
