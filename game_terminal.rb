# coding: utf-8
# frozen_string_literal: true

require 'artii'
require 'highline/import'

require './bin/initial.rb'

include Oyun

TITLE = Artii::Base.new


ACILIS_NOTU = $GAMEDATA["#{$SETTINGS[:lang]}"][:opening]

def oyuna_basla
  oyun_intro
  oyuncu = yeni_oyuncu
  harita = yeni_harita
  bolum_kaza_alani = Motor.new(oyuncu, harita)
  bolum_kaza_alani
end

def oyun_intro
  sleep 1
  puts 'UYANIŞ'.center(70, '*').yellow
  puts "\n"
  ACILIS_NOTU.each_char do |k|
	  print k.green
	  sleep 0.1
  end
end

puts TITLE.asciify('Gölge Oyunu')
puts "A Game By Gökhan Çağlar\n\n".red

choose do |menu|
  menu.prompt = "Gölge Oyunu | Seçiminiz >> "

  menu.choice(:yeni) { oyuna_basla }
  menu.choices(:exit) { exit }
end
