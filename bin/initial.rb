# coding: utf-8
# frozen_string_literal: true

require 'yaml'
require 'colorize'

$GAMEDATA = YAML.load(File.open('./data/game.yaml'))
$SETTINGS = YAML.load(File.open('./data/settings.yaml'))

# Oda nesneleri
module OdaNesneleri

  class UcBoyutluYazici

    attr_reader :model, :urunler, :durum, :cikti

    def initialize
      @model = 'MaxIcan'
      @urunler = {
                       0=>{:isim=>:levye, :gerekenler=>{:demir=>2, :krom=>1}},
                       1=>{:isim=>:kablo, :gerekenler=>{:plastik=>1, :bakir=>1}},
                       2=>{:isim=>:elektronik_devre, :gerekenler=>{:plastik=>3, :bakir=>2, :lehim=>2}},
                       3=>{:isim=>:pil, :gerekenler=>{:cinko=>3, :lityum=>1, :elektronik_devre=>1, :kablo=>2}}
                     }

      @durum = true
      @cikti = nil
    end

    def etkilesim(oyuncu)
      item_uretici(oyuncu)
    end

    private def item_uretici(oyuncu)
      puts "Temel nesne üretim modülüne hoşgeldiniz!
      Lütfen bekleyin! 'Kütle Biçimleyici Çanta'daki nesne verileri işlenecek...
      İşlem başarılı!
      Nesne ve nicelik bilgileri Hash:
        #{oyuncu.canta}
      Lütfen üretmek istediğniz nesnenin sayısal kodunu giriniz!"
      puts "Üretilebilirler HASH:
           #{@urunler}"
      print 'Seçiminiz >> '
      secim = gets.to_i

      uret(oyuncu, secim, @urunler)
    end

    private def uret(oyuncu, secim, kvargs)
      tezgah = {}
      kvargs.dig(secim, :gerekenler).each_key do |key|
        tezgah[key] = kvargs.dig(secim, :gerekenler, key) if oyuncu.canta[key]&. >= kvargs.dig(secim, :gerekenler)
    end

     if tezgah == kvargs[secim][:gerekenler]
      tezgah.each_pair do |key, value|
        oyuncu.canta[key] -= value
      end
      tezgah = {}
      puts "Bir #{kvargs[secim][:isim]} üretildi!"
      oyuncu.canta.delete_if {|key, value| value == 0}
      @cikti = kvargs.dig(secim, :isim)
      return @cikti
    end
    puts "Kaynaklar yetersiz!"
   end
  end

  class RobotUnitesi
    def initialize
      @model = "Celik-DMKK"
      @durum = false
      @gecmis_cikti_raporu = []
    end

    def etkilesim(oyuncu)
      if @durum
        robot_uretici(oyuncu)
      else
        ariza
        puts "Onarım prosedürü deneniyor!"
        onar(oyuncu)
      end
    end


    def robot_uretici(oyuncu)
        oyuncu.gorevitemleri.dig(:blueprints, :droids).each_pair do |key, value|
          oyuncu.cantaya_ekle(key)
          @gecmis_cikti_raporu << key
        end
    end

    def tekrar_uret(oyuncu)
      puts 'Üretilecek Droid Kodu >>> '
      @gecmis_cikti_raporu.each_with_index do |item, index|
        puts "#{item} : #{index}"
      end
      secim = gets.to_i
      oyuncu.cantaya_ekle(@gecmis_cikti_raporu[secim])
    end

    def ariza
      puts 'Robot Unitesi Sistem Durumu:
      Çevre Donanım: ERİŞİLEMEZ
      Çekirsek Isısı: TEHLIKE!
      SİSTEM KAPATILDI!'
      puts 'Onarmak için Robot Unit Blueprint Gerekli!'
    end

    def onar(oyuncu)
      oyuncu.gorevitemleri.dig(:blueprints, :tools).include(:robout_unit) ? @durum = true :
        'Onarmak için "Robot Unit Blueprint" Gerekli!'
      puts "Kaçış kabini Droid Üretim Modülüne hoşgeldiniz!"
      puts "Uyarı sistemi çökmüş durumda! Çevre donanımlara erişim: FALSE"
      puts "Çekirdek Isı Değeri KRİTİK hesaplayıcı ünite çalıştırılamaz!"
    end
  end
end


# Oyun kontrolleri
module Oyun

  def yeni_harita
    harita = Harita.new
  end

  def kaydet(oyun, yol: './data/', filename: nil)
    if filename.nil?
      print 'Dosya adı >> '
      filename = gets.chomp
    end
    nesne = YAML.dump(oyun)
    File.open("#{yol}Save-#{filename}.yaml", 'w') do |f|
      f.write(nesne)
    end
    'Saved!'
  end

  def yukle(filename)
    f = File.open(filename, 'r')
    nesne = YAML.load(f)
    nesne
  end

  def cantaya_ekle(sembol)
    canta.key?(sembol) ? canta[sembol] += 1 : canta[sembol] = 1
  end

  def yeni_oyuncu(filename: './data/default.yaml')
    kvargs = YAML.load(File.open(filename, 'r'))
    oyuncu = Oyuncu.new(**kvargs)
    oyuncu
  end

  def yeni_dusman(filename: './data/enemy.yaml', tip: 'minion')
    dusmanlar = YAML.load(File.open(filename, 'r'))
    kvargs = dusmanlar[tip.to_sym]
    dusman = Dusman.new(**kvargs)
    dusman
  end

  def zar_at
    salla
  end

  private def salla(kere: 2, yuzeyler: 20)
    degerler_dizisi = []
    kere.times { degerler_dizisi << rand(0..yuzeyler) }
    nihai = degerler_dizisi.inject(:+) # Uyumluluk icin sum kullanmadim.
    puts "
         #{@veriler[:isim]} zar attı:
           #{degerler_dizisi[0]} +  #{degerler_dizisi[1]} = #{nihai}
         "
    nihai
  end

  def basari_orani(value)
    katsayi = case value
              when (32..40) then 4
              when (25..31) then 3
              when (18..24) then 2
              when (11..17) then 1
              when (0..10)  then 0
              else
                'hata'
              end
    katsayi
  end
end

# Oyuncu nesnesinin tanimlamalari
class Oyuncu
  attr_accessor :veriler, :canta, :x_coord, :y_coord, :gorevitemleri
  attr_reader :id, :zar, :basari_katsayisi

  def initialize(**bilgiler)
    @id = bilgiler[:id]
    @veriler = bilgiler[:veriler]
    @zar = 0
    @basari_katsayisi = 0
    @canta = {}
    @gorevitemleri = {}
    @x_coord, @y_coord = 0, 0
  end

  def hayatta_mi?
    return true unless @veriler[:saglik] <= 0
    game_over
  end

  def game_over
    puts "#{@veriler[:isim]} acı içinde öldü! R.I.P. #{Time.now.year}"
  end

  def darbe_aldi(hasar)
    @veriler[:saglik] -= hasar if hasar > 0
    @veriler[:saglik]
  end

  def iyilestir(sifa)
    @veriler[:saglik] += sifa if @veriler[:saglik] < 100
    @veriler[:saglik]
  end

  def saldir
    atak = basarim * @veriler[:silah][:atak]
  end

  def basarim
    @zar = zar_at
    @basari_katsayisi = basari_orani(@zar)
  end

  def oyuncunun_durumu
    puts "\n\n"
    puts "Oyuncu".center(40, "*")
    puts "İsim: #{@veriler[:isim]}".blue
    puts "Sağlık: #{@veriler[:saglik]}".red
    puts "Silah: #{@veriler[:silah][:model]}".yellow
    puts "Atak: #{@veriler[:silah][:atak]}".red
    puts "Savunma: #{@veriler[:giyim][1]}".green
    puts "*" * 40
  end
end

# Item sinifi
class Item
  attr_accessor :malzeme

  def initialize(item_adi)
    @malzeme = item_adi
  end

  def etkilesim(oyuncu)
    if @malzeme == :medkit
      oyuncu.iyilestir(25)
    else
      oyuncu.cantaya_ekle(@malzeme)
    end
  end

  def to_s
    "Bir #{@malzeme} buldum."
  end
end

class GorevItemi
  def initialize(item_adi)
    @item_adi = item_adi.to_sym
  end
end

# Dusman sinifi
class Dusman < Oyuncu
  def initialize(**kvargs)
    super
  end

  def etkilesim(oyuncu)
    puts "Saldırmak için hamleni yaptın!"
    while oyuncu.hayatta_mi?
      # Oyuncu Hamlesi
      hamle(oyuncu, self)
      puts "Kalan Canı : #{self.veriler[:saglik]}"
      break unless self.veriler[:saglik] > 0
      sleep 0.10
      # Dusman Hamlesi
      puts "#{self.veriler[:isim]} saldırmak için hamlesini yaptı!"
      hamle(self, oyuncu)
      break unless oyuncu.veriler[:saglik] > 0
    end
  end

  def hamle(oyuncu, dusman)
    atak_gucu = oyuncu.saldir
    atak_gucu += oyuncu.veriler[:silah][:atak] * 0.5 if oyuncu.basari_katsayisi > oyuncu.veriler[:silah][:basari]
    if atak_gucu > 0
      puts "#{oyuncu.veriler[:isim]} tam da #{atak_gucu} şiddetinde vurdu!"
      dusman.darbe_aldi(atak_gucu)
    else
      puts "#{oyuncu.veriler[:isim]} ISKALADI!"
    end
  end
end

# Karakterler
class NPC < Oyuncu
  def initialize
    super
  end
end

class Harita

  BOLUM_X = 5
  BOLUM_Y = 5

  attr_accessor :odalar

  def initialize
    @odalar = [[KaraKutu, Koridor, Koridor, Tuzak, Atelye],
               [Koridor, Koridor, Revir, Koridor, Koridor],
               [Koridor, Koridor, Koridor, Cephanelik, Tuzak],
               [Koridor, Cekirdek, Tuzak, Koridor, Koridor],
               [Koridor, Koridor, Koridor, BuyukSavas, CikisKapisi]]
  end

  def nesneyi_kuzeye_oynat(nesne)
    nesne.y_coord -= 1 if nesne.y_coord > 0
  end

  def nesneyi_guneye_oynat(nesne)
    nesne.y_coord += 1 if nesne.y_coord < BOLUM_Y - 1
  end

  def nesneyi_batiya_oynat(nesne)
    nesne.x_coord += 1 if nesne.x_coord < BOLUM_X - 1
  end

  def nesneyi_doguya_oynat(nesne)
    nesne.x_coord -=1 if nesne.x_coord > 0
  end

  def konumunu_al(nesne)
   @odalar[nesne.x_coord][nesne.y_coord].new
  end
end

class Atelye
  # Yazılacak
end

class Cephanelik
  # Yazılacak
end

class Cekirdek
  # Yazılacak
end

class BuyukSavas
  # Yazılacak  
end

class CikisKapisi
  # Yazılacak
end

class KaraKutu
  include OdaNesneleri

  attr_reader :tip, :alan, :aciklama
  attr_accessor :workbench, :droidbench

  ETKILESIMLER = [:u, :r, :d]
  def initialize
    @tip = :oda
    @alan = '16 metrekare'
    @workbench = UcBoyutluYazici.new
    @droidbench = RobotUnitesi.new
    @aciklama = <<~HEREDOC
    Soğuk ve zehirli gaz sızıntısı uyarısı aktif. Yanık plastik
    kokuyor. Odada bir adet 3 boyutlu nesne üretim ünitesi ve bir
    adet droid ünitesi var. Ayrıca güvenlik kabininden yanma ve uğultu
    sesleri geliyor, çarpma anında parçalanmış olmalı. Üretim ünitesi
    ile hayyatta kalmayı sağlayacak materyalleri temin etmek mümkün.
    Bir adet droid üretim ünitesi bulunmakta.
    Kaçış kaibininin güney ve doğu yönlerinde kapılar var.
    Batı  be juzey çarpışma anında tamamen ezilmiş.
    HEREDOC
  end

  def etkilesim(oyuncu)
    puts @aciklama.red
    puts "Ne yapacağıma karar vermeliyim
    1- Kuzeydeki 3 Boyutlu Nesne Üretim Ünitesini kullan(u)
    2- Batıdaki Droid üretim ünitesini kullan(r)
    Odanın çıkış kapıları doğu ve güneyde olmalı
    3- Gezinmeye devam et(k)".yellow

    secim = oyuncu_secimini_al
    eylem(secim, oyuncu)
  end

  def oyuncu_secimini_al
    print "Komut #{ETKILESIMLER}>> ".green
    gets.chomp
  end

  def eylem(secim, oyuncu)
    case secim
    when '1', 'u'
      @workbench.etkilesim(oyuncu)
    when '2', 'r'
      @droidbench.etkilesim(oyuncu)
    when '3', 'k'
      puts 'Bir sonraki hamleme karar vermem gerekli!'
    else
      puts "Odadan çıkmak için doğu ya da güneye ilerlemeliyim"
    end
  end  
end



class Koridor
  ITEMLER = [:medkit, :bakir, :demir, :celik, :elektronik_devre, :hurda, :kablo, :plastik, :pil]
  attr_reader :tip
  attr_accessor :nitelik, :icerik, :sifat

  def initialize
    @tip = :koridor
    @icerik = icerik_al
    @nitelik = nitelik_al
    @sifat = sifat_al
  end

  def etkilesim(oyuncu)
    if @icerik
      @icerik.etkilesim(oyuncu)
      @icerik = nil
    end
  end

  def icerik_al
    nesne_varyasyon = [Item, :dusman].sample
    nesne = nesne_varyasyon == Item ? Item.new(ITEMLER.sample) : yeni_dusman
    nesne
  end

  def nitelik_al
    ['Çürümüş et kokan', 'Taze kan kokan', 'pas ve yanmış kablo kokan', 'gaz sızıntısı sesi gelen'].sample
  end

  def sifat_al
    ['ürpertici bir sessizlikteki', 'uğultulu', 'makine gürültüsü ile sarsılan', 'patlama ve kısa devre sesleri gelen'].sample
  end

  def to_s
  "#{@nitelik} ve #{@sifat} bir bölümündesin koridorun. Tehlikelere karşı tetikte ol"
  end
end

class Revir
  def initialize
    @tip      = :revir
    @alan     = "12 metrekare"
    @aciklama = <<~HEREDOC
    Kaçış gemisinin sadece temel işlevlere sahip tıbbi destek
    unitesi. Hala fonksiyonel durumda görünüyor. Benim türüm
    için yapabilecekleri maalesef oldukça kısıtlı.
    HEREDOC
  end

  def etkilesim(oyuncu)
    oyuncu.veriler[:saglik] = 100
    puts "Temel hayati fonksiyonlar onarıldı.
          Uyarı! Çevre etkileşim unitileri:
          ONARILAMAZ DURUMDA!"
  end
end

class Tuzak
  attr_reader :tip, :hasar, :aciklama

  def initialize(hasar, aciklama)
    @tip = :tuzak
    @hasar = rand(5..60)
    @aciklama = 'Güvenilk ekibi kendini koruyabilmek için tuzak kurmuş olmalı.'
  end

  def etkilesim(oyuncu)
    puts "Lanet olası bir tuzak! #{@aciklama}"
    oyuncu.etkilesim(@hasar)
  end
end

class Motor
  HAMLELER = [:tara, :kuzey, :bati, :guney, :dogu, :exit]
  KISALTMALAR = %i(t k b g d e)

  def initialize(player, levelsinifi)
    @player = player
    @world = levelsinifi
    oyunu_baslat
  end

  private def oyunu_baslat
    while @player.hayatta_mi?
      @gecerli_konum = @world.konumunu_al(@player)
      @player.oyuncunun_durumu

      eylem = oyuncu_komutunu_al
      if eylem == :exit then break end
      unless HAMLELER.include?(eylem) || KISALTMALAR.include?(eylem)
        next
      end
      eylemi_al(eylem)
    end
  end

  def oyuncu_komutunu_al
    print "Komut #{HAMLELER}>> "
    gets.chomp.to_sym
  end

  def eylemi_al(eylem)
    case eylem
    when :tara, :t
      konumu_tara
    when :kuzey, :k
      @world.nesneyi_kuzeye_oynat(@player)
      konumu_tara
    when :bati, :b
      @world.nesneyi_batiya_oynat(@player)
      konumu_tara
    when :dogu, :d
      @world.nesneyi_doguya_oynat(@player)
      konumu_tara
    when :guney, :g
      @world.nesneyi_guneye_oynat(@player)
      konumu_tara
    when :exit, :e
      puts 'Tekrar görüşeceğiz!'
      exit
    else
      puts 'Ne dedin?'
    end
  end

  def konumu_tara
    puts "\n\n"
    puts "<<Koordinatlar>>".center(40, "*").red
    puts "YATAY: #{@player.x_coord}".blue
    puts "DİKEY: #{@player.y_coord}".yellow
    puts "Tarama Sonucu: #{@gecerli_konum.to_s}".green
    puts "*" * 40
    puts "\n"
    @gecerli_konum.etkilesim(@player)
  end
end
